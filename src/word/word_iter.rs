use crate::counter::Counter;
use crate::util::VectorAccess;

pub struct WordIterator<'a> {
    source: VectorAccess<'a, char>,
    counter: Vec<usize>
}

impl<'a> WordIterator<'a> {
    pub fn new(source: VectorAccess<'a, char>) -> Self {
        Self {
            source,
            counter: Vec::new()
        }
    }
}

impl Iterator for WordIterator<'_> {
    type Item = String;

    fn next(&mut self) -> Option<String> {
        if self.source.get().len() == 0 {
            if self.counter.len() == 0 {
                // Cannot use increase here: initial >= rollover.
                // This is a magic value for saying we already yielded the empty word.
                self.counter.push(0);
                Some(String::from(""))
            } else {
                None
            }
        } else {
            let result = self.counter.to_word(self.source.get());
            Counter::increase(&mut self.counter, 0, self.source.get().len());
            Some(result)
        }
    }
}

#[cfg(test)]
mod tests {
    use more_asserts::*;
    use super::*;

    fn create_access<'a>() -> VectorAccess<'a, char> {
        VectorAccess::Direct(vec!['a', 'b', 'c'])
    }

    fn create_empty_access<'a>() -> VectorAccess<'a, char> {
        VectorAccess::Direct(Vec::new())
    }

    #[test]
    fn test_create_iterator() {
        let access = create_access();
        let _iter = WordIterator::new(access);
    }

    #[test]
    fn test_obtain_empty_word() {
        let access = create_access();
        let mut iter = WordIterator::new(access);
        assert_eq!("", iter.next().unwrap());
    }

    #[test]
    fn test_ends_after_empty_word_on_empty_source() {
        let access = create_empty_access();
        let mut iter = WordIterator::new(access);
        assert_eq!("", iter.next().unwrap());
        assert!(iter.next().is_none());
    }

    #[test]
    fn test_stays_none() {
        let access = create_empty_access();
        let mut iter = WordIterator::new(access);
        iter.next();
        assert!(iter.next().is_none());
        assert!(iter.next().is_none());
        assert!(iter.next().is_none());
        assert!(iter.next().is_none());
        assert!(iter.next().is_none());
    }

    #[test]
    fn test_nonempty_alphabet_does_not_end() {
        let access = create_access();
        let mut iter = WordIterator::new(access);

        // Infinity is hard to reach... this will have to be enough
        for _ in 0..10000 {
            assert!(!iter.next().is_none());
        }
    }

    #[test]
    fn test_word_size_monotonically_increases() {
        let access = create_access();
        let mut iter = WordIterator::new(access);

        // Infinity is hard to reach... this will have to be enough
        let mut last_len = 0;
        for _ in 0..10000 {
            let next = iter.next().unwrap();
            assert_ge!(next.len(), last_len);
            last_len = next.len();
        }
    }

    #[test]
    fn test_abcba_is_word() {
        let access = create_access();
        let mut iter = WordIterator::new(access).take(1000);
        let needle = iter.find(|x| x == "abcba");
        assert!(!needle.is_none());
    }

    #[test]
    fn test_aaaaaa_is_word() {
        let access = create_access();
        let mut iter = WordIterator::new(access).take(1000);
        let needle = iter.find(|x| x == "aaaaaa");
        assert!(!needle.is_none());
    }
}
pub enum VectorAccess<'a, T> {
    Direct(Vec<T>),
    Indirect(&'a Vec<T>)
}

impl<'a, T> VectorAccess<'a, T> {
    pub fn get(&self) -> &Vec<T> {
        match &self {
            VectorAccess::Direct(v) => &v,
            VectorAccess::Indirect(v) => v
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_access_direct() {
        let mut vec: Vec<u64> = Vec::new();
        vec.push(5);

        let access = VectorAccess::Direct(vec);
        assert_eq!(1, access.get().len());
        assert_eq!(5, access.get()[0]);
    }

    #[test]
    fn test_get_direct_same() {
        let vec: Vec<u64> = Vec::new();
        let access = VectorAccess::Direct(vec);

        let ptr_a = access.get() as *const Vec<u64>;
        let ptr_b = access.get() as *const Vec<u64>;
        assert_eq!(ptr_a, ptr_b);
    }

    #[test]
    fn test_access_indirect() {
        let mut vec: Vec<u64> = Vec::new();
        vec.push(5);

        let access = VectorAccess::Indirect(&vec);
        assert_eq!(1, access.get().len());
        assert_eq!(5, access.get()[0]);
    }

    #[test]
    fn test_get_indirect_same() {
        let vec: Vec<u64> = Vec::new();
        let access = VectorAccess::Indirect(&vec);

        let ptr_a = access.get() as *const Vec<u64>;
        let ptr_b = access.get() as *const Vec<u64>;
        assert_eq!(ptr_a, ptr_b);
    }
}